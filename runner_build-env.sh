##########################################
#  VARIABLES                             #
##########################################
echo "Starting installation"
printenv

# Create Hot Session Runner User
shell_user=${shell_user:="dtu_training"}
# useradd --comment 'Hot Session Runner User' --create-home $shell_user --shell /bin/bash

# These need to be set as environment variables prior to launching the script
#export DT_ENV_URL=
#export DT_API_TOKEN=       
#export DT_PAAS_TOKEN=     
#export REGISTRATION_TOKEN=

##########################################
#  DO NOT MODIFY ANYTHING IN THIS SCRIPT #
##########################################

echo "Installing packages"
apt-get update -y 
apt-get install -y git vim
snap install docker
chmod 777 /var/run/docker.sock
apt install curl -y 
snap install jq

##########################################
#  Gitlab Download and Install           #
##########################################
home_folder="/home/$shell_user"

# Download the binary for your system
curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
# Give it permissions to execute
chmod +x /usr/local/bin/gitlab-runner
# Create a GitLab CI user
useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
# Install and run as service
gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
gitlab-runner start
gitlab-runner register --non-interactive --name ansible_hot_runner --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN --tag-list docker,linux,ansible,hot_session --executor docker --docker-image ruby:2.6

###########################################
# SSH Keys create
###########################################
mkdir $home_folder/.ssh/
ssh-keygen -t rsa -b 4096 -N "" -C "Runner" -f "$home_folder/.ssh/id_rsa" -q 

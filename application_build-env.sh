##########################################
#  VARIABLES                             #
##########################################
echo "Starting installation"
printenv

# Create Hot Session Applicaiton User
shell_user=${shell_user:="dtu_training"}

# These need to be set as environment variables prior to launching the script
#export DT_ENV_URL=
#export DT_API_TOKEN=       
#export DT_PAAS_TOKEN=     
#export REGISTRATION_TOKEN=

##########################################
#  DO NOT MODIFY ANYTHING IN THIS SCRIPT #
##########################################

echo "Installing packages"
apt-get update -y 
apt-get install -y git vim
apt install curl -y 
# INSTALL JAVA FOR EASYTRAVEL
apt install default-jre -y
apt install openjdk-11-jdk-headless

##########################################
#  Set Home Folder                       #
##########################################
home_folder="/home/$shell_user"

###########################################
# SSH Create Keys to SSH
###########################################
mkdir $home_folder/.ssh/
ssh-keygen -t rsa -b 4096 -N "" -C "Application" -f "$home_folder/.ssh/id_rsa" -q 

################################
# EasyTravel                   #
################################
cd $home_folder
mkdir easytravel
cd "$home_folder/easytravel"
wget https://etinstallers.demoability.dynatracelabs.com/latest/dynatrace-easytravel-linux-x86_64.jar -O $home_folder/easytravel/easytravel.jar
chmod +x $home_folder/easytravel
java -jar easytravel.jar -y
cd $home_folder/easytravel/easytravel-2.0.0-x64/
nohup ./runEasyTravelNoGUI.sh &>/dev/null &
